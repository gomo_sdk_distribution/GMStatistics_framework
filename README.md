# GOMOStatistics

[![CI Status](https://img.shields.io/travis/ZDerain/GOMOStatistics.svg?style=flat)](https://travis-ci.org/ZDerain/GOMOStatistics)
[![Version](https://img.shields.io/cocoapods/v/GOMOStatistics.svg?style=flat)](https://cocoapods.org/pods/GOMOStatistics)
[![License](https://img.shields.io/cocoapods/l/GOMOStatistics.svg?style=flat)](https://cocoapods.org/pods/GOMOStatistics)
[![Platform](https://img.shields.io/cocoapods/p/GOMOStatistics.svg?style=flat)](https://cocoapods.org/pods/GOMOStatistics)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

- above `ios 8.0`

## Installation
GOMOStatistics is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GOMOStatistics',:git => 'https://gitlab.com/gomo_sdk_distribution/GMStatistics_framework.git'
```

##How to Use

-  导入`SDK头文件`

```objc
#import <GOMOStatistics/GOMOStatistics.h>
```

-  配置不同协议的功能点

```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
        // 设置上传的服务器 (默认上传正式服)
    [GOMOStatistics setEnvironment:StaitsticsEnvironment];
        
    // 是否输出日志 (默认不输出)
    [GOMOStatistics setEnableLog:StaitsticsEnableLog];
    
   /** 推荐在此时统一配置统计SDK协议需要的功能点, 以及产品ID; 便于集中管理 (无需手动初始化)
     *
     *  此时以 GoLive iOS产品为例
     *  支付协议功能点 916
     *  推广协议功能点 791
     *  用户行为统计协议功能点 826
     *  用户实时行为统计协议功能点 827
     */
     [GOMOStaitstics configurePaymentFuncId:@"916" disseminationFuncId:@"791" userRealTimeBehaviorFuncId:@"827" userNormalBehaviorFuncId:@"826" adFuncId:@"188"];
    
    // 单独设置功能点
    [GOMOStatistics configureUserNormalBehaviorFuncId:@"1533"];
    [GOMOStatistics configurePaymentFuncId:[GOMOStatisticsDeviceInfo getBundleId]];
        

    // 注意: 初始化(需在配置功能点后初始化) 
    [GOMOStaitstics setup:@"1396397626"];
     return YES;
 }
```

- 埋点统计

    - 具体信息请看`GOMOStaitstics.h`

```objc
    NSString *sampleCode = @"测 试 数 据";
     
    // 上传用于信息
    [GOMOStaitstics uploadUserInfoStatisticsIfNeeded];
    
    // 插入用户操作记录
    [GOMOStaitstics insertUserStatisticOperationCode:(@15).description statisticsObject:sampleCode associationObject:sampleCode tab:sampleCode entrance:sampleCode remark:sampleCode position:sampleCode];
    
    // 实时上传用户操作记录
    [GOMOStaitstics postUserRealtimeStatisticOperationCode:sampleCode statisticsObject:sampleCode associationObject:sampleCode tab:sampleCode remark:sampleCode position:sampleCode];
    
    // 其他具体协议使用, 请看头文件...
```

- 获取当前应用信息

    - 详情见`GOMOStatisticsDeviceInfo.h`

```objc
    // goid
    [GOMOStatisticsDeviceInfo getGOID];
    
    // bundle id
    [GOMOStatisticsDeviceInfo getPackageName];
```

- 版本说明

    - `0.4.6`

        - 修复`104协议`添加`tab参数`, 处理版本号/版本名错乱问题

    - `0.4.7`

        - 修复头文件暴露错误问题

    - `0.4.8`

        - 修复`19协议上传失败, 再次启动没有以及补传机制`

        - 修复`App版本`升级/降级, 没有及时上传问题

    - `0.4.9`

        - `静态常量`保存数据, 替换为`NSUserDefault`保存, 减少内存分配

    - `0.5.0`

        - 添加`统计SDK``未初始化就使用`, 抛出异常

    - `0.5.1`

        - 修复找不到分类方法BUG

    - `0.5.2`

        - 统计SDK`Debug`模式下, 才抛出异常

    - `0.5.3`

        - 优化一些变量命名, 以及性能优化

    - `0.5.4`

        - 修复`NSLog`打印信息不全`bug`

    - `0.5.5`

        - 修复`101协议``Posintion`字段传空`bug`

        - `59协议`, 暴露`Position`字段, 请及时替换`API`

    - `0.5.6`

        - 修复`59协议`上传`数据错位BUG`

    - `0.5.7`

        - 优化代码结构, 去除`59协议方法 order字段`
   - `0.5.8`
        - 监听网络状态, 无网络情况下; 直接缓存数据, 不予上传

    - `0.5.9`

        - 替换`105`协议中`产品ID`字段为`功能点ID`

## Author

Derain-zhou, derain-zhou@foxmail.com

## License

GOMOStatistics is available under the MIT license. See the LICENSE file for more info.


